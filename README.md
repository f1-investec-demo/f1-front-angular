# F1 Front Demo App

## Intro

A sample site built with [Angular 12](https://angular.io/) & [Bootstrap](https://getbootstrap.com/) that shows Formula One (F1) seasons from 2010 till now. Data is polled from the [Ergast Developer API](http://ergast.com/mrd/)  through a proxy server.

Site can be demo-ed at https://f1-front-demo.web.app/

Project is built with npm v14 as mentioned in the `.nvmrc` file.

## Setup

**Prerequisite**

- Have node (version ^14.x.x) and npm (version ^ 7.x.x) installed* (https://nodejs.org/en/)
- Have angular installed globally (https://angular.io/guide/setup-local#install-the-angular-cli)
- Have yarn installed globally (https://yarnpkg.com/lang/en/docs/cli/install/)
- Have sass install (dart varient)  (https://sass-lang.com/install)
- Have NVM install (optional, for easier switching of node/npm versions)

** Angular requires an* [*active LTS or maintenance LTS*](https://nodejs.org/about/releases) *version of Node.js.*

**Local setup**

- Clone repo
- Change directory to cloned repo
- Update the local `apiBaseUrls` key in the `environment/environment.ts` file with to https://f1-proxy.demoview.co.za/
- Run `yarn` (in terminal)
- Run `yarn start`


## Contribution Guide


## Styling

Use BEM methodology, see documentation for more details (http://getbem.com/)

**Commits**
Follow the Conventional Commit convention, see documentation for more details (https://www.conventionalcommits.org/en/v1.0.0/)



## Credits

**Card Design**

https://dribbble.com/shots/10977528-Formula-1-App-Standings-and-Driver-Details


**Formula One Website**

https://www.formula1.com/en/toolbar/guidelines.html


F1 Logo (https://upload.wikimedia.org/wikipedia/commons/3/33/F1.svg)


## Copyright

All rights belong their respective holders.


## Caveats

**Images**
Image where sourced from the F1 website.

Images found are for current season (2021) drivers, therefor driver’s in the older older seasons might not have associated images.

**API Proxy**

Site runs a proxy server that handles caching of results, results are cached for 24 hours.
This was done to ease the load on the Ergast API and is recommended by the developer and ensure all calls are returned secured by redirecting request to HTTPS.

In addition all request are returned as JSON Format with MRData.

URL endpoints match the Ergast API with only the first two url segments being replaced,
so http://ergast.com/api/f1/2008/5/results would be http://f1-proxy.demoview.co.za/api/proxy/2008/5/results



## Roadmap
- Enforce commit message formatting by using commit linter and commit hooks
- Enforce CSS styling through linter
- Client side caching 
- Session state for theme switcher
- Transform data on proxy server to include driver images
- Include a map/diagram of the tracks for the various locations
- Adding event to calendar
- Adding driver birthday to calendar
- Generate Angular Interfaces by using XML Schema definition.
- Mobile nav should updates


