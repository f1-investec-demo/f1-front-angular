export const environment = {
  production: true,
  apiBaseUrls: {
    "general": "https://f1-proxy.demoview.co.za/api/",
    "f1": "https://f1-proxy.demoview.co.za/api/proxy/",
  }
};
