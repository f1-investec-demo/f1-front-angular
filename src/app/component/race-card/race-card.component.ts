import {Component, Input, OnInit} from '@angular/core';
import {Country} from "../../services/country/country.interface";
import {CountryService} from "../../services/country/country.service";
import * as _ from 'lodash';
import * as dayjs from "dayjs";
import * as utc from "dayjs/plugin/utc";
import * as timezone from "dayjs/plugin/timezone";
import {RaceResultsService} from "../../services/race-results/race-results.service";

@Component({
  selector: 'app-race-card',
  templateUrl: './race-card.component.html',
  styleUrls: ['./race-card.component.scss'],
  providers: [ CountryService ]
})
export class RaceCardComponent implements OnInit {

  @Input() race: any;
  countryStore:Country[] = [];
  countryCode:string = '';
  raceResults:any = [];
  pollPositions = [];
  roundLabelString = '';

  constructor(private countryService: CountryService,
              private raceResult: RaceResultsService) { }

  ngOnInit(): void {
    this.countryCode = this.getCountryCode(this.race.Circuit.Location.country);
    this.getResults();

  }

  getCountryCode(country: any): string {
    let countrySearch = _.find(this.countryStore, {'name': country});

    if(countrySearch !== undefined) {
      this.countryCode = country;
    }else{
      this.countryService.getCountry(country).subscribe(data => {
        this.countryStore.push(data);
        this.countryCode = data.code.toLowerCase();
      }, error => {
        console.error(`Country: ${country} not found`);
      });
    }

    return this.countryCode;
  }

  roundLabel(round: any) {
    this.roundLabelString = (round === "1") ?  'Testing' : 'Round ' + (round - 1);
  }

  // Join date and time and localize it to current timezone
  concatenateDate(date: string, time: string) {
    dayjs.extend(utc);
    dayjs.extend(timezone);

    const dateTimez = date + ' ' + time;

    return dayjs.tz(dateTimez, 'UTC').format('YYYY-MM-DD HH:mm:s');
  }

  getResults() {
    this.raceResult.get(this.race.season, this.race.round).subscribe((data) => {

      this.raceResults = data;
      this.roundLabel(this.race.round);
      this.extractPollPositions();

    });
  }

  extractPollPositions() {
    const races = this.raceResults.RaceTable.Races;

    if(races.length === 0 ) return;

    const racePoll = races[0].Results;

    this.pollPositions = _.take(racePoll, 3);
  }
}
