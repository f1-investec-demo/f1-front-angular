import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-driver-card',
  templateUrl: './driver-card.component.html',
  styleUrls: ['./driver-card.component.scss']
})
export class DriverCardComponent implements OnInit {

  @Input() type: string = 'full';
  @Input() driver:any;
  @Input() poll: any = false;
  @Input() winner:any;

  constructorLogo: any;
  givenName: any;
  familyName: any;
  classes:any = [];

  driverImage: any;
  teamConstructor: any;
  isWinner = false;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.setValues();
    this.setWinner();
    this.setClasses();
  }

  setValues() {

    if(this.poll) {
      this.driver = this.poll.Driver;
    }

    // fetch the image
    this.http.get(environment.apiBaseUrls.general + 'driver-images?familiy-name=' +this.driver.familyName +'&given-name=' + this.driver.givenName)
      .subscribe((data:any) => {
        if(data.data.length) {
          this.driverImage = '/assets/drivers/' + data.data[0].file;
        }
      });

    this.teamConstructor = this.poll.Constructor.constructorId;
  }

  setWinner() {

    if( this.winner !== undefined && (this.driver.code === this.winner.code) ) {
      this.isWinner = true;
    }
  }

  setClasses() {
    this.classes.push(`driver-card--${this.type}`);

    if(this.isWinner) {
      this.classes.push('driver-card--winner');
    }
  }

}
