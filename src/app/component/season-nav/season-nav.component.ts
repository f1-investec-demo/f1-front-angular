import {Component, Input, OnInit, Renderer2} from '@angular/core';
import * as _ from 'lodash';
import {FormControl} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-season-nav',
  templateUrl: './season-nav.component.html',
  styleUrls: ['./season-nav.component.scss']
})
export class SeasonNavComponent implements OnInit {
  seasons: number[] | undefined;
  @Input() seasonForYears: number = 12;

  theme = 'dark';
  toppings = new FormControl();

  constructor(
    private renderer: Renderer2,
    private router: Router) { }

  ngOnInit(): void {
    this.createYears();
  }

  createYears() {

    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();

    this.seasons = _.range(currentYear, currentYear -this.seasonForYears, -1);
  }

  changeTheme() {

    const currentTheme = this.theme;
    this.theme = (this.theme == 'dark') ? 'light' : 'dark';
    const bodyDom = this.renderer.selectRootElement('body', true);
    this.renderer.removeClass(bodyDom, currentTheme);
    this.renderer.addClass(bodyDom, this.theme);
  }

  routeToNavItem($event: any) {
    console.log($event)
    this.router.navigate(['season',$event.value]);
  }
}
