import {Injectable, EventEmitter, Renderer2, RendererFactory2} from '@angular/core';
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  private renderer: Renderer2;
  state: Subject<boolean> = new Subject<boolean>();

  // Store the truthy state of the service (true == is loading, false == is not loading)
  loadingState = false;

  constructor(private rendererFactory: RendererFactory2) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  toggle() {
    this.state.next(!this.loadingState);
  }

  loading() {
    this.loadingState = true;
    this.state.next(this.loadingState)
  }

  complete() {
    this.loadingState = false;
    this.state.next(this.loadingState);
  }

  updateLoaderDom(status:boolean) {
    const loaderDom = this.renderer.selectRootElement('.loader', true);

    if(status) {
      this.renderer.removeClass(loaderDom,'hide');
    }else{
      this.renderer.addClass(loaderDom, 'hide');
    }

  }


}
