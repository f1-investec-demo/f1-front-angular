import { Injectable } from '@angular/core';
import * as dayjs from "dayjs";
import * as utc from "dayjs/plugin/utc";
import * as timezone from "dayjs/plugin/timezone";

@Injectable({
  providedIn: 'root'
})
export class DateService {
  private _date:any;
  private _time:any;
  private _timezone:string = 'UTC';

  constructor() {
    dayjs.extend(utc);
    dayjs.extend(timezone);
  }

  set date(date: string) {
    this._date = date;
  }

  get date() {
    return this._date;
  }

  set time(time: string) {
    this._time = time;
  }

  get time() {
    return this._time;
  }

  set timezone(timezone: string) {
    this._timezone = timezone;
  }

  get timezone () {
    return this._timezone;
  }

  // Join date and time and localize it to current timezone
  localizeDateTime(format = 'YYYY-MM-DD HH:mm:s') {
    const dateTime = this.date + ' ' + this.time;

    return dayjs.tz(dateTime, this._timezone).format(format);
  }

  localizeDate() {
    return dayjs.tz(this.date, this._timezone);
  }

  localizeTime() {
    return dayjs.tz(this.time, this._timezone);
  }
}
