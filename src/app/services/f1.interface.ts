export interface RaceSchedule {
  xmlns: string,
  series: string,
  limit: number,
  offset: number,
  url: string,
  RaceTable: RaceTable
}

export interface LocationModel {
  lat: number,
  long: number,
  locality: number,
  country: string
}

export interface Circuit {
  circuitId: string,
  url: string,
  circuitName: string,
  Location: LocationModel
}

export interface Races {
  round: number,
  url: string,
  raceName: string,
  date: string,
  time: string,
  season: string,
  Circuit: Circuit
}

export interface RaceTable {
  season: number,
  Races: Races[],
  Circuit: Circuit,
  Results: Results[],
  raceName: string,
}

export interface Constructor {
  constructorId: string,
  name: string,
  nationality: string,
  url: string,
}

export interface Driver {
  driverId: string,
  permanentNumber: number,
  code: string,
  url: string,
  givenName: string,
  familyName: string,
  dateOfBirth: string,
}

export interface AverageSpeed {
  units: string,
  speed: number,
}

export interface Time {
  time: string
  millis: number,
}

export interface FastestLap {
  AverageSpeed: AverageSpeed,
  Time: Time,
  lap: number,
  rank: number,
}

export interface Results {
  Constructor: Constructor,
  Driver: Driver,
  FastestLap: FastestLap,
  Time: Time,
  grid: number,
  laps: number,
  points: number,
  position: number,
  positionText: number,
  status: string
}


