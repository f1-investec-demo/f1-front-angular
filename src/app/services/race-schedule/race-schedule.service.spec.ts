import { TestBed } from '@angular/core/testing';

import { RaceScheduleService } from './race-schedule.service';

describe('SeasonService', () => {
  let service: RaceScheduleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RaceScheduleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
