import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { RaceSchedule } from "../f1.interface";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class RaceScheduleService {

  season = 2021;

  constructor(private http: HttpClient) { }

  getSeasons(season:number): Observable<RaceSchedule> {
    this.season = season;
    return this.http.get<RaceSchedule>(environment.apiBaseUrls.f1 + this.season)
  }
}
