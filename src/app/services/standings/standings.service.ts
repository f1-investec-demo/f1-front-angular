import { Injectable } from '@angular/core';
import {CacheService} from "../cache/cache.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class StandingsService {

  constructor(private cache: CacheService, private http: HttpClient) { }

  get(season:any) {
    const cacheName = season + '-driver-standings';

    if(this.cache.exists(cacheName)) {
      return this.cache.get(cacheName);
    }

    return this.http.get(environment.apiBaseUrls.f1 + season + '/driverStandings');
  }

}
