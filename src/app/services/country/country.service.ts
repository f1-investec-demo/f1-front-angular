import { Injectable } from '@angular/core';
import {RaceSchedule} from "../f1.interface";
import {environment} from "../../../environments/environment";
import {Country} from "./country.interface";
import {HttpClient} from "@angular/common/http";
import * as _ from 'lodash';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  countries: Observable<any> | undefined;

  constructor(private http: HttpClient) { }

  getCountries() {
    return this.http.get<Country>(environment.apiBaseUrls.general + 'countries')
  }

  getCountry(country: string) {
    return this.http.get<Country>(environment.apiBaseUrls.general + 'countries/' + country)
  }

}
