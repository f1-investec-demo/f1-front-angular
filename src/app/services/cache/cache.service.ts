import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import * as _ from "lodash";

@Injectable({
  providedIn: 'root'
})
export class CacheService {

  private _data: any;

  constructor() { }

  store(name:string, results: any) {

    let cacheData:any = {'name': name, 'data': results};

    this._data.push(cacheData);
  };

  exists(name:string) {
    return _.some(this._data,{'name': name});
  }

  get(name:string) {

    return new Observable((observer)  => {

      if(!this.exists(name)) observer.error(`CacheService: cache data not defined ${name}`);

      const cacheData = _.find( this._data, {'name': name});

      observer.next( cacheData.data );
      observer.complete();
    });
  }
}
