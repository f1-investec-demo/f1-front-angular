import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { CacheService } from "../cache/cache.service";

@Injectable({
  providedIn: 'root'
})
export class RaceResultsService {

  constructor(private http: HttpClient, private cache: CacheService) { }

  get(year: any, round: any) {
    const cacheName = year + '-' + round + '-results';

    if( this.cache.exists(cacheName) ) {
      return this.cache.get(cacheName);
    }

    return this.http.get(environment.apiBaseUrls.f1 + `${year}/${round}/results`);
  }

}
