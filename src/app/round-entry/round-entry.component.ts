import { Component, OnInit } from '@angular/core';
import { RaceResultsService } from "../services/race-results/race-results.service";
import { ActivatedRoute } from "@angular/router";
import {DateService} from "../services/date/date.service";
import {StandingsService} from "../services/standings/standings.service";

@Component({
  selector: 'app-round-entry',
  templateUrl: './round-entry.component.html',
  styleUrls: ['./round-entry.component.scss']
})
export class RoundEntryComponent implements OnInit {
  season: any;
  round: any;
  raceResultStore: any;
  race: any;
  standingsStore: any;
  seasonWinner:any

  constructor(private raceResult: RaceResultsService,
              private route: ActivatedRoute,
              private date: DateService,
              private standings: StandingsService
  ) { }

  ngOnInit(): void {

    this.route.paramMap.subscribe(paramMap => {

      this.season = paramMap.get('year');
      this.round = paramMap.get('round');

      this.raceResult.get(this.season, this.round).subscribe(data => {
        this.raceResultStore = data;

        if(this.raceResultStore.RaceTable.Races.length) {
          this.race = this.raceResultStore.RaceTable.Races[0];
        }

      });

      this.standings.get(this.season).subscribe((data) => {
        this.standingsStore = data;
        this.seasonWinner = this.standingsStore.StandingsTable.StandingsLists[0].DriverStandings[0].Driver;
      });

    });
  }

  localDateTime() {
    this.date.date = this.race.date;
    this.date.time = this.race.time;

    return this.date.localizeDateTime();
  }

}
