import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule, routeComponents } from './app-routing.module';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { NavComponentComponent } from './nav-component/nav-component.component';
import { HomeComponent } from './home/home.component';
import { DriverCardComponent } from './component/driver-card/driver-card.component';
import { SeasonNavComponent } from "./component/season-nav/season-nav.component";
import { RaceScheduleComponent } from './race-schedule/race-schedule.component';
import { RaceCardComponent } from './component/race-card/race-card.component';

import { OrdinalPipe } from './pipes/ordinal/ordinal.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSliderModule} from "@angular/material/slider";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatOptionModule} from "@angular/material/core";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    routeComponents,
    SeasonNavComponent,
    NavComponentComponent,
    HomeComponent,
    DriverCardComponent,
    RaceScheduleComponent,
    RaceCardComponent,
    OrdinalPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,
    ReactiveFormsModule,
  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
