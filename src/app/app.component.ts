import {Component, OnInit} from '@angular/core';
import {CountryService} from "./services/country/country.service";
import {Country} from "./services/country/country.interface";
import {LoaderService} from "./services/loader/loader.service";
import {NavigationStart, Router, RouterEvent} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'f1-front';

  constructor(private loader: LoaderService, private router: Router) {
    router.events.subscribe((event) => {
      if (event instanceof NavigationStart){
        this.loader.updateLoaderDom(true);
      }else{
        this.loader.updateLoaderDom(false);
      }
    })
  }

  ngOnInit() {

  }
}
