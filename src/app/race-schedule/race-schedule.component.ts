import { Component, OnInit } from '@angular/core';
import {ActivatedRoute } from "@angular/router";
import { RaceScheduleService } from "../services/race-schedule/race-schedule.service";
import { Races, RaceSchedule } from "../services/f1.interface";
import {RaceResultsService} from "../services/race-results/race-results.service";

@Component({
  selector: 'app-race-schedule',
  templateUrl: './race-schedule.component.html',
  styleUrls: ['./race-schedule.component.scss'],
  providers: [RaceScheduleService]
})
export class RaceScheduleComponent implements OnInit {

  races: Races[] | undefined;
  season: any;

  constructor(private raceSchedule: RaceScheduleService,
              private raceResults: RaceResultsService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(paramMap => {
      // @ts-ignore
      this.season = paramMap.get('year')
      this.getSeasons();
    })

    this.getSeasons();
  }

  getSeasons() {

    this.raceSchedule.getSeasons( this.season ).subscribe((data: RaceSchedule) => {
      this.races = data.RaceTable.Races;
      this.races.map( race => {
        this.getResults(this.season, race.round);
      });
    });
  }

  // TODO: Check call as data not being used!!!
  getResults(year: any, round: any) {
    this.raceResults.get(year, round).subscribe(data => {
    });
  }


}
