import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SeasonsComponent} from "./seasons/seasons.component";
import {RoundEntryComponent} from "./round-entry/round-entry.component";

const routes: Routes = [
  {path: '', redirectTo: 'season/2021', pathMatch: 'full' },
  {path: 'season/:year', component: SeasonsComponent, pathMatch: 'full' },
  {path: 'season/:year/:round', component: RoundEntryComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routeComponents = [
  SeasonsComponent,
  RoundEntryComponent
];
